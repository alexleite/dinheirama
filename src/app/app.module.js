(function() {
    'use strict';

    angular.module('app', [
        'app.core',
        //
        'app.services',
        'app.auth',
        'app.welcome',
        'app.game'
    ])
})();