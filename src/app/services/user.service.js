(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('UserService', UserService);
    
    UserService.$inject = ["$firebaseObject", "$firebaseArray", "$q"]

    function UserService($firebaseObject, $firebaseArray, $q) {

        var COIS_PER_HOUR = 10
        var FUN_PER_HOUR = 10
        var FEED_PER_HOUR = 10

        function checkExperiencePoints(user, experience) {
            var experienceLimit = user.level * 25;
            var newExperience = experience + user.experience
            if(newExperience >= experienceLimit){
                user.level = user.level + 1;
                user.experience = newExperience - experienceLimit;
            }else{
                user.level = user.level;
                user.experience = newExperience;
            }
        }

        return {
            createUser: function(user){
                var userData = {
                    uid: user.uid,
                    email: user.email,
                    experience: 0,
                    level: 1,
                    funny: 100,
                    feeding: 100,
                    coins: 100
                }
                return firebase.database().ref('users/' + userData.uid).set(userData)
            },
            getUserByUid: function(uid){
                return $firebaseObject(firebase.database().ref().child("users").child(uid))
            },
            getUserToysByUid: function(uid){
                return $firebaseArray(firebase.database().ref().child("users_toys").child(uid))
            },
            getUserInvestimentsByUid: function(uid){
                return $firebaseArray(firebase.database().ref().child("users_investiments").child(uid))
            },
            eat: function(user, food) {
                var feeding = food.points + user.feeding
                if(feeding > 100) { feeding = 100 }
                
                user.feeding = feeding
                user.coins -= food.coins
                checkExperiencePoints(user, food.experience)
                return user.$save()
            },
            buyToy: function(user, toy) {
                user.coins -= toy.coins                
                checkExperiencePoints(user, toy.experience)
                var newChildRef = firebase.database().ref('users_toys/' + user.uid).push()
                var toyAux = angular.copy(toy)
                toyAux.durability = 100
                newChildRef.set(toyAux)
                return user.$save()
            },
            play: function(user, list, toy) {
                var promises = []
                var defferedUser = $q.defer()
                promises.push(defferedUser)                
                var defferedToy = $q.defer()
                promises.push(defferedToy)

                var funny = toy.points + user.funny
                user.funny = (funny > 100) ?  100: funny

                user.$save().then(function(resp){
                    defferedUser.resolve('ok')
                })
                toy.durability = toy.durability - 5
                list.$save(toy).then(function(resp){
                    defferedToy.resolve('ok')
                })

                return $q.all(promises)
            },
            donate: function(user, list, toy) {
                var promises = []
                var defferedUser = $q.defer()
                promises.push(defferedUser)                
                var defferedToy = $q.defer()
                promises.push(defferedToy)

                checkExperiencePoints(user, 50)
                user.$save().then(function(resp){
                    defferedUser.resolve('ok')
                })
                list.$remove(toy).then(function(resp){
                    defferedToy.resolve('ok')
                })

                return $q.all(promises)
            },
            invest: function(user, investiment, coins) {
                user.coins -= coins 
                checkExperiencePoints(user, investiment.experience)
                                
                var newChildRef = firebase.database().ref('users_investiments/' + user.uid).push()
                var newInvestiment = {
                    coins: coins,
                    experience: investiment.experience,
                    yield: investiment.yield,
                    lack: investiment.lack,
                    name: investiment.name
                }
                if(investiment.risk){
                    newInvestiment.risk = investiment.risk
                }
                newChildRef.set(newInvestiment)
                return user.$save()                
            },
            withdraw: function(user, list, investiment) {
                var promises = []
                var defferedUser = $q.defer()
                promises.push(defferedUser)                
                var defferedInvestiment = $q.defer()
                promises.push(defferedInvestiment)

                user.coins += investiment.coins
                user.$save().then(function(resp){
                    defferedUser.resolve('ok')
                })
                list.$remove(investiment).then(function(resp){
                    defferedInvestiment.resolve('ok')
                })

                return $q.all(promises)
            },
            updateStatus: function(user){
                var promises = []
                var defferedUser = $q.defer()
                promises.push(defferedUser)                
                var defferedInvestiment = $q.defer()
                promises.push(defferedInvestiment)

                if(!user.updateDate){
                    user.updateDate = new Date().getTime()
                }
                var currentDate = new Date().getTime()
                var hours = Math.floor(Math.abs(user.updateDate - currentDate) / 36e5)
                
                for (var i = 0; i < hours; i++) {
                    user.coins += COIS_PER_HOUR
                    user.funny = (user.funny - FUN_PER_HOUR < 0) ? 0 : user.funny - FUN_PER_HOUR 
                    user.feeding = (user.feeding - FEED_PER_HOUR < 0) ? 0 : user.feeding - FUN_PER_HOUR
                    
                    var investiments = $firebaseArray(firebase.database().ref().child("users_investiments/"+user.uid))
                    
                    investiments.$loaded(function(){
                        var investimentsPromises = []
                        angular.forEach(investiments, function(investiment, key){                            
                            var deffered = $q.defer()
                            investimentsPromises.push(deffered)
                            var coinsYield = Math.round((investiment.coins * investiment.yield) / 100)
                            var coins 
                            if (investiment.riks && Math.rand(0, 100) > 75) {
                                coins = investiment.coins - coinsYield;
                                if (coins < 0) {
                                    coins = 0                                
                                }
                            } else {
                                coins = investiment.coins + coinsYield
                                checkExperiencePoints(user, investiment.experience)
                            }
                            investiment.coins = coins
                            investiment.lack = (investiment.lack > 0) ? investiment.lack - 1 : 0
                            investiments.$save(investiment).then(function (){
                                deffered.resolve('ok')
                            })
                        })
                        $q.all(investimentsPromises).then(function(){
                            defferedInvestiment.resolve('ok')
                        })
                    })
                }    
                if(hours > 0){
                    user.updateDate = currentDate
                }
                user.$save().then(function(resp){
                    defferedUser.resolve('ok')
                })
                
                return $q.all(promises) 
            }

        }
    }

})();