(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('Auth', Auth);
    
    Auth.$inject = ["$firebaseAuth", "$firebaseObject"]

    function Auth($firebaseAuth, $firebaseObject) {
        return $firebaseAuth()
    }

})();