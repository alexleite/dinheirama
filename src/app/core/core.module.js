angular.module('app.core', [
    'ngStorage',
    'ngAnimate',
    'ngResource',
    'ngMessages',
    'ngSanitize',
    'ngMaterial',
    'ui.router',
    'angular.filter',
    'templates',
    'firebase'
]);