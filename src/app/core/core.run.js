(function (angular) {
    'use strict';

    angular
        .module('app.core')
        .run(CoreRun);

    CoreRun.$inject = ["$rootScope", "$state"];

    function CoreRun($rootScope, $state) {

        $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
            if (error === "AUTH_REQUIRED") {
                $state.go("login");
            }
        });

        $rootScope.showLoader = false
        
    }

})(angular);
