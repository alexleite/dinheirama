(function () {
    'use strict';

    angular
        .module('app.game')
        .controller('RestaurantController', RestaurantController);

    RestaurantController.$inject = ["$rootScope","$scope", "$state", "currentAuth", "UserService", "$mdToast"]

    function RestaurantController($rootScope, $scope, $state, currentAuth, UserService, $mdToast) {

        $scope.user

        init()
        function init(){
            $rootScope.showLoader = true
            $scope.user = UserService.getUserByUid(currentAuth.uid)
            
            $scope.user.$loaded(function(data){
                $scope.user = data
                $rootScope.showLoader = false
            })
        }

        $scope.list = [
            {
                name: "Água",
                experience: 4,
                points: 4,
                coins: 4,
            },
            {
                name: "Almoço",
                experience: 12,
                points: 10,
                coins: 10,
            },
            {
                name: "Bala",
                experience: 1,
                points: 1,
                coins: 1,
            },
            {
                name: "Café da Manhã",
                experience: 8,
                points: 7,
                coins: 7,
            },
            {
                name: "Café da Tarde",
                experience: 8,
                points: 7,
                coins: 7,
            },
            {
                name: "Chiclete",
                experience: 2,
                points: 2,
                coins: 2,
            },
            {
                name: "Jantar",
                experience: 12,
                points: 10,
                coins: 10,
            },
            {
                name: "Lanche",
                experience: 5,
                points: 5,
                coins: 5,
            },
            {
                name: "Pão",
                experience: 3,
                points: 3,
                coins: 3,
            },
            {
                name: "Pirulito",
                experience: 2,
                points: 2,
                coins: 2,
            }
        ]

        $scope.eat = function (item) {
            UserService.eat($scope.user, item).then(function(resp){
                $mdToast.show(
                    $mdToast
                        .simple()
                        .textContent('+' + item.points + ' pontos de alimentação')
                )
            })
        }

    }





})();
