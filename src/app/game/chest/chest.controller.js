(function () {
    'use strict';

    angular
        .module('app.game')
        .controller('ChestController', ChestController);

    ChestController.$inject = ["$rootScope", "$scope", "$state", "currentAuth", "UserService", "$mdToast"]

    function ChestController($rootScope, $scope, $state, currentAuth, UserService, $mdToast) {

        $scope.user
        $scope.list = []
        $scope.ready = false

        init()
        function init(){
            $rootScope.showLoader = true
            $scope.user = UserService.getUserByUid(currentAuth.uid)
            
            $scope.user.$loaded(function(data){
                $scope.user = data
                $scope.list = UserService.getUserToysByUid(currentAuth.uid)
                $scope.list.$loaded(function(data){
                    $scope.ready = true
                    $rootScope.showLoader = false
                })
            })
        }

        $scope.donate = function(toy){
            UserService.donate($scope.user, $scope.list, toy).then(function(resp){
                $mdToast.show(
                    $mdToast
                        .simple()
                        .textContent('Parabéns seu brinquedo foi doado. +50 pontos de experiência')
                )
            })
        }

        $scope.play = function(toy){
            UserService.play($scope.user, $scope.list, toy).then(function(resp){
                $mdToast.show(
                    $mdToast
                        .simple()
                        .textContent('+' + toy.points + ' pontos de diversão')
                )                
            })
        }

    }

})();
