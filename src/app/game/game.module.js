(function () {
    'use strict';

    angular
        .module('app.game', [])
        .config(config);


    function config($stateProvider) {
        $stateProvider.state(
            'game', {
                url: '/game',
                templateUrl: 'app/game/game.html',
                controller: 'GameController',
                resolve: {
                    "currentAuth": ["Auth", function (Auth) {
                        return Auth.$requireSignIn()
                    }]
                }
            })        
            .state('game.chest', {
                url: '/chest',
                templateUrl: 'app/game/chest/chest.html',
                controller: 'ChestController',
                resolve: {
                    "currentAuth": ["Auth", function (Auth) {
                        return Auth.$requireSignIn()
                    }]
                }
            })     
            .state('game.investments', {
                url: '/investments',
                templateUrl: 'app/game/investments/investments.html',
                controller: 'InvestmentsController',
                resolve: {
                    "currentAuth": ["Auth", function (Auth) {
                        return Auth.$requireSignIn()
                    }]
                }
            })        
            .state('game.restaurant', {
                url: '/restaurant',
                templateUrl: 'app/game/restaurant/restaurant.html',
                controller: 'RestaurantController',
                resolve: {
                    "currentAuth": ["Auth", function (Auth) {
                        return Auth.$requireSignIn()
                    }]
                }
            })       
            .state('game.store', {
                url: '/store',
                templateUrl: 'app/game/store/store.html',
                controller: 'StoreController',
                resolve: {
                    "currentAuth": ["Auth", function (Auth) {
                        return Auth.$requireSignIn()
                    }]
                }
            })
    }

})();