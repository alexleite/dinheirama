(function () {
    'use strict';

    angular
        .module('app.game')
        .controller('StoreController', StoreController);

    StoreController.$inject = ["$rootScope", "$scope", "$state", "currentAuth", "UserService", "$mdToast"]

    function StoreController($rootScope, $scope, $state, currentAuth, UserService, $mdToast) {

        $scope.user
        
        init()
        function init(){
            $rootScope.showLoader = true
            $scope.user = UserService.getUserByUid(currentAuth.uid)
            
            $scope.user.$loaded(function(data){
                $scope.user = data
                $rootScope.showLoader = false
            })
        }

        $scope.list = [
            {
                name: "Bola",
                experience: 5,
                coins: 20,
                points: 5,
            },
            {
                name: "Boneca",
                experience: 5,
                coins: 20,
                points: 5,
            },
            {
                name: "Skate",
                experience: 25,
                coins: 90,
                points: 15,
            },
            {
                name: "Bicicleta",
                experience: 30,
                coins: 150,
                points: 20,
            },
            {
                name: "Vídeo Game",
                experience: 40,
                coins: 180,
                points: 30,
            }
        ]

        $scope.buyToy = function (item) {
            UserService.buyToy($scope.user, item).then(function(resp){
                $mdToast.show(
                    $mdToast
                        .simple()
                        .textContent('Brinquedo comprado com sucesso!')
                )
            })
        }

    }





})();
