(function () {
    'use strict';

    angular
        .module('app.game')
        .controller('GameController', GameController);

    GameController.$inject = ["$rootScope", "$scope", "$state", "currentAuth", "UserService"]

    function GameController($rootScope, $scope, $state, currentAuth,  UserService) {
        
        var loadingStatus = false
        $scope.user

        init()
        function init(){
            if($state.$current.name == 'game'){
                $state.go("game.chest")
                $scope.activeTab = 0
            }
            $scope.user = UserService.getUserByUid(currentAuth.uid)
        }
        
        $scope.$watch(function(){
            return $state.$current.name
        }, function(newVal, oldVal){
            if(newVal == 'game.chest'){
                $scope.activeTab = 0
            }else if(newVal == 'game.store'){
                $scope.activeTab = 1
            }else if(newVal == 'game.restaurant'){
                $scope.activeTab = 2
            }else if(newVal == 'game.investments'){
                $scope.activeTab = 3
            }
            
            $scope.user.$loaded(function(){
                if(!loadingStatus){
                    loadingStatus = true
                    UserService.updateStatus($scope.user).then(function(){
                        loadingStatus = false
                    })
                }
            })
        })

    }

        
            
        

})();
