(function () {
    'use strict';

    angular
        .module('app.game')
        .controller('InvestmentsController', InvestmentsController);

    InvestmentsController.$inject = ["$rootScope", "$scope", "$state", "currentAuth", "UserService", "$mdDialog", "$mdToast"]

    function InvestmentsController($rootScope, $scope, $state, currentAuth, UserService, $mdDialog, $mdToast) {

        $scope.listBankInvestiments = [
            {
                name: "Ações",
                yield: 15, // rendimento
                risk: true,
                lack: 5, // carencia
                experience: 10
            },
            {
                name: "CDB",
                yield: 5,
                lack: 10,
                experience: 3
            },
            {
                name: "Poupança",
                yield: 2,
                lack: 0,
                experience: 2
            },
            {
                name: "Tesouro Nacional",
                yield: 8,
                lack: 20,
                experience: 5,
            }
        ]

        $scope.listUserInvestiments = []

        init()
        function init(){
            $rootScope.showLoader = true
            $scope.user = UserService.getUserByUid(currentAuth.uid)

            $scope.user.$loaded(function(){
                $scope.listUserInvestiments = UserService.getUserInvestimentsByUid(currentAuth.uid)
                $scope.listUserInvestiments.$loaded(function(){
                    $rootScope.showLoader = false
                })
            })
        }

        $scope.invest = function(investiment, ev){
            var confirm = $mdDialog.prompt()
                .title('Investir em ' + investiment.name)
                .placeholder('Digite a quantidade de moedas')
                .ariaLabel('Digite a quantidade de moedas')
                .targetEvent(ev)
                .ok('Investir!')
                .cancel('Cancelar');
        
            $mdDialog.show(confirm).then(function(coins) {
                if(!isNumeric(coins)){
                    $mdToast.show(
                        $mdToast
                            .simple()
                            .textContent('Você não possui moedas suficientes')
                    )
                }else{
                    coins = parseInt(coins)
                    if($scope.user.coins < coins){
                        $mdToast.show(
                            $mdToast
                                .simple()
                                .textContent('Você não possui moedas suficientes')
                        )
                    }else{
                        UserService.invest($scope.user, investiment, coins).then(function(){
                            $mdToast.show(
                                $mdToast
                                    .simple()
                                    .textContent('Investimento efetuado com sucesso')
                            )
                        })
                    }
                }
            });
        }

        $scope.withdraw = function(investiment){
            UserService.withdraw($scope.user, $scope.listUserInvestiments, investiment).then(function(resp){
                $mdToast.show(
                    $mdToast
                        .simple()
                        .textContent('Investimento retirado! ' + (investiment.coins> 0 ? '+' + investiment.coins :'-' + investiment.coins) + ' moedas')
                )
            })
        }

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

    }





})();
