(function () {
    'use strict';

    angular
        .module('app.game')
        .controller('WelcomeController', WelcomeController);

    WelcomeController.$inject = ["$scope", "$state", "currentAuth", "UserService"]

    function WelcomeController($scope, $state, currentAuth, UserService) {
        
        $scope.form 
        
        $scope.saveProfile = saveProfile
        $scope.user

        init()
        function init(){
            $scope.form = {}
            $scope.user = UserService.getUserByUid(currentAuth.uid)
            
            $scope.user.$loaded(function(data){
                $scope.user = data
                if($scope.user.avatar){
                    $state.go("game.chest")
                }
            })
            loadListAvatars()
        }

        function loadListAvatars(){
            $scope.listAvatars = []
            var boy, girl
            for(var i = 0; i <= 23; i++){
                $scope.listAvatars.push("boy-" + i + ".svg")
                $scope.listAvatars.push("girl-" + i + ".svg")
            }
        }

        function saveProfile(){
            $scope.user.nickname = $scope.form.nickname
            $scope.user.avatar = $scope.form.avatar

            $scope.user.$save().then(function(ref) {
                $state.go("game.chest")                        
            }, function(error) {
                console.log("Error:", error);
            })
        }

    }

        
            
        

})();