(function () {
    'use strict';

    angular
        .module('app.welcome', [])
        .config(config);


    function config($stateProvider) {
        $stateProvider.state(
            'welcome', {
                url: '/welcome',
                templateUrl: 'app/welcome/welcome.html',
                controller: 'WelcomeController',
                resolve: {
                    "currentAuth": ["Auth", function (Auth) {
                        return Auth.$requireSignIn()
                    }]
                }
            }
        )
    }

})();