(function () {
    'use strict';

    angular
        .module('app.auth.register', [])
        .config(config);

    function config($stateProvider) {
        // State
        $stateProvider.state('register', {
            url: '/auth/register',
            templateUrl: 'app/auth/register/register.html',
            controller: 'RegisterController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return !Auth.$requireSignIn();
                }]
            }
        });

    }

})();