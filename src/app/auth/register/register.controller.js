(function () {
    'use strict';

    angular
        .module('app.auth.register')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ["$scope", "$state", "Auth", "UserService"]

    function RegisterController($scope, $state, Auth, UserService) {
        $scope.form = {}

        $scope.createNewAccount = createNewAccount

        function createNewAccount() {
            
            Auth.$createUserWithEmailAndPassword($scope.form.email, $scope.form.password)
                    .then(function (firebaseUser) {
                UserService.createUser(firebaseUser).then(function(resp){
                    $state.go("welcome")                    
                })
            }).catch(function (error) {
                console.log("Authentication failed:", error)
            })

        }
    }
})();