(function () {
    'use strict';

    angular
        .module('app.auth.login', [])
        .config(config);

    function config($stateProvider) {
        // State
        $stateProvider.state('login', {
            url: '/auth/login',
            templateUrl: 'app/auth/login/login.html',
            controller: 'LoginController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return !Auth.$requireSignIn();
                }]
            }
        });

    }

})();