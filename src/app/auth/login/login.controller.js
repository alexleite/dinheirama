(function () {
    'use strict';

    angular
        .module('app.auth.login')
        .controller('LoginController', LoginController);

    LoginController.$inject = ["$scope", "$state", "Auth", "UserService"]

    function LoginController($scope, $state, Auth, UserService) {
        
        $scope.form = {}
        $scope.error

        $scope.authWithEmail = authWithEmail
        $scope.authWithSocialMedia = authWithSocialMedia

        function authWithEmail() {
            Auth.$signInWithEmailAndPassword($scope.form.email, $scope.form.password).then(function (userAuth){
                var user = UserService.getUserByUid(userAuth.uid) 
                user.$loaded(function(data){
                    user = data
                    if(user.avatar){
                        $state.go("game.chest")
                    }else{
                        $state.go("welcome")
                    }
                })       
            }).catch(function (error) {
                $scope.error = error.message
                console.log("Authentication failed:", error)
            })
        }

        function authWithSocialMedia(socialMedia) {
            Auth.$signInWithPopup(socialMedia).then(function (firebaseUser) {
                var user = UserService.getUserByUid(firebaseUser.user.uid)                
                user.$loaded(function(data){
                    user = data
                    if(user.avatar){
                        $state.go("game.chest")
                    }else{
                        if(!user.uid){
                            UserService.createUser(firebaseUser.user).then(function(resp){
                                $state.go("welcome")
                            })
                        }else{
                            $state.go("welcome")
                        }
                    }
                })
            }).catch(function (error) {
                $scope.error = error.message
                console.log("Authentication failed:", error)
            })
        }

    }
})();